-- Please send issues or fork here: 
-- https://bitbucket.org/manubing/lua-notifo
-------------------------------------
-- Send a Notifo notification
--
-- @param message		The message to send
-- @param to				Your notifo username
-- @param title			Optional name of "notification event
-- @param label			Optional label describing the "application"
-- @param uri 			Optional uri that will be loaded when the notification is 
--									opened; if a web address, must start with 
--									http:// or https://
-------------------------------------
function notifo_notification (message, title, to, label, uri)
	--	put here your api key
	local api_key = 'your-api-key'
	
	local to 		= to or 'your-notifo-username'
	local label = label or 'Vera'

	local https = require 'ssl.https'
	local ltn12 = require 'ltn12'

	-- utility method to make text URL friendly
	function url_encode(str)
	  if (str) then
	    str = string.gsub (str, "\n", "\r\n")
	    str = string.gsub (str, "([^%w ])",
	        function (c) return string.format ("%%%02X", string.byte(c)) end)
	    str = string.gsub (str, " ", "+")
	  end
	  return str   
	end

	local request_body = 'msg=' .. url_encode(message) .. '&to=' .. to .. '&label=' .. label
	if (title) then request_body = request_body .. '&title=' .. url_encode(title) end
	if (uri) then request_body = request_body .. '&uri=' .. url_encode(uri) end
	local response_body = {}

	b, c, h = https.request {
	  url = 'https://api.notifo.com/v1/send_notification',
	  method = 'POST',
	  headers = {
	    ["Content-Type"] =  "application/x-www-form-urlencoded",
			["Content-Length"] = string.len(request_body),
			["Authorization"] = "Basic " .. (mime.b64(to .. ":" .. api_key))
		},
	  source = ltn12.source.string(request_body),
	  sink = ltn12.sink.table(response_body),
	}

end

-- Usage examples :
--notifo_notification('test fom a function')
--notifo_notification('extended test message', 'message title')
